__build_extension__ = True
ext_name = __ext_name__ = "mgmt_ui_extension"
ext_version = __ext_version__ = "0.0.1"
stage = __stage__ = "BEFORE_BUILD"

import logging
import subprocess
import time
import libctbuilder as ctb


def do(*args, **kwargs):
    config = kwargs[ctb.KEY_MASTER_CONFIG]
    ui_root = config['extensions']['mgmt']['ui_build']['ui_root']
    proc = subprocess.Popen('cd %s;npm install;bower install;grunt build;' % ui_root, shell=True,
                            stderr=subprocess.PIPE, stdout=subprocess.PIPE)

    for line in iter(proc.stdout.readline, b''):
        print line
    out, err = proc.communicate()
    print out, err

    if proc.returncode != 0:
        logging.error('Failed building UI')
        print
        """
          Failed Building UI, please assure that you have those utils:
             1. npm (yum install npm)
             2. bower (npm install bower -g)
             3. grunt (npm install grunt-cli -g)
        """
        raise Exception('Failed building UI')



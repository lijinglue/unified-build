"""
This is a builder extension for go project dependencies
"""
__build_extension__ = True
ext_name = __ext_name__ = "go_dependency_extension"
ext_version = __ext_version__ = "0.0.1"
stage = __stage__ = "BEFORE_BUILD"

import logging
import subprocess
import os
import time
import libctbuilder as ctb

GO_PATH = os.environ['GOPATH']
GO_ROOT = os.environ['GOROOT']


def do(*args, **kwargs):
    config = kwargs[ctb.KEY_MASTER_CONFIG]
    if ctb.KEY_DEPENDENCIES in config:
        deps = config[ctb.KEY_DEPENDENCIES]
        for dep in deps:
            try:
                if not dep_exists(config, dep):
                    print("installing %s ..." % dep)
                    proc = subprocess.Popen('cd $GOPATH; go get %s' % dep, shell=True, stderr=subprocess.PIPE,
                                            stdout=subprocess.PIPE)
                    for line in iter(proc.stdout.readline, b''):
                        print line
                    proc.communicate()
                    if proc.returncode != 0:
                        raise Exception()
            except Exception as e:
                logging.error('Failed to fetch dependency: %s' % dep, e)
                raise


#TODO: pkg md5 checking, etc
def dep_exists(config, dep_name):
    if 'go_src_root' in config:
        src = GO_PATH + '/%s' % config['go_src_root']
    else:
        src = GO_PATH + '/src'
    return os.path.exists(src + '/%s' % dep_name) or \
           os.path.exists(GO_ROOT + '/src/pkg/%s' % dep_name)


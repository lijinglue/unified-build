__build_extension__ = True
ext_name = __ext_name__ = "mgmt_conf_extension"
ext_version = __ext_version__ = "0.0.1"
stage = __stage__ = "BEFORE_BUILD"
import subprocess


def do(*args, **kwargs):
    proc = subprocess.Popen(
        '[ -d  /etc/clustertech/chess-mgmt/ ] || mkdir -p /etc/clustertech/chess-mgmt/;\
         cp -rf $GOPATH/etc/* /etc/clustertech/chess-mgmt/',
        shell=True,
        stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    for line in iter(proc.stdout.readline, b''):
        print line
    out, err = proc.communicate()
    print out, err
    if proc.returncode != 0:
        raise Exception('Failed install config files')

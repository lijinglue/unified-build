#!/usr/bin/env python

import json
import os
import sys
import logging
import hashlib
import os.path
import imp
import traceback
import subprocess
import optparse
import libctbuilder as ctb


def dual_list(list_a, list_b, callback):
    """
    Cast operation on 2 list with a callback func
    return another result as a list
    """
    result = []
    for a in list_a:
        for b in list_b:
            result.append(callback(a, b))
    return result


BUILD_MASTER_CONFIG = 'build.json'
BUILD_EXTENSION_FOLDER = 'extensions'

BUILTIN_STAGES = ["VALIDATE", "BUILD", "TEST", "PACKAGE", "INSTALL", "DEPLOY", "CLEAN"]
STAGE_HOOKS = ["BEFORE", "AFTER"]
SUPPORTED_STAGES = dual_list(STAGE_HOOKS, BUILTIN_STAGES, lambda x, y: '%s_%s' % (x, y))


def load_module(code_path):
    """
    load python code on the fly
    """
    try:
        try:
            fin = open(code_path, 'rb')
            return imp.load_source(hashlib.md5(code_path).hexdigest(), code_path, fin)
        finally:
            try:
                fin.close()
            except:
                pass
    except ImportError, x:
        traceback.print_exc(file=sys.stderr)
        raise
    except:
        traceback.print_exc(file=sys.stderr)
        raise


class GoBuildLifeCycleManager(object):
    """
    Manage build in stages, invoke hooks in extensions dir if any
    """

    extensions = {}
    failed_extensions = {}

    def __init__(self):
        current_path = os.path.realpath(__file__)
        current_dir = os.path.dirname(current_path)
        master_file = current_dir + '/../' + BUILD_MASTER_CONFIG;
        extension_dir = current_dir + '/' + BUILD_EXTENSION_FOLDER
        if os.path.exists(master_file):
            try:
                self.config = json.load(open(BUILD_MASTER_CONFIG))
            except Exception as e:
                logging.info('Failed to Load Master')
                logging.error(e)
                raise e

        for subdir, dirs, files in os.walk(extension_dir):
            for f in files:
                if not f.endswith('.py'):
                    continue
                try:
                    extension = load_module(extension_dir + '/' + f)
                    if not hasattr(extension, '__build_extension__'):
                        continue
                    if extension.stage not in SUPPORTED_STAGES:
                        raise Exception(
                            'Stage claimed not supported, please use one of those: %s' % ' '.join(SUPPORTED_STAGES))

                    if self.extensions is None:
                        self.extensions = {}

                    if not extension.stage in self.extensions:
                        self.extensions[extension.stage] = {}

                    if not extension.ext_name in self.extensions[extension.stage]:
                        self.extensions[extension.stage][extension.ext_name] = extension
                    else:
                        raise Exception('Extension Name Confilict: %s' % extension.name)

                except Exception as e:
                    logging.exception(e)

    def download_extensions(self):
        """
        TODO: download extensions from a respository,
              Failover on multiple respository
        """
        pass

    def build(self, config=None, options=None):
        """
        The compile/build stage
        This is now specified for go
        """
        main_file = config[ctb.KEY_MAIN_FILE]
        main_pkg = os.path.dirname(main_file)
        proc = subprocess.Popen('cd %s; go build -x' % main_pkg, shell=True, stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)
        for line in iter(proc.stdout.readline, b''):
            print line
        out, err = proc.communicate()
        print out, err

        if proc.returncode != 0:
            raise Exception("Build Failed")
            logging.error("Build Failed")

    def test(self, config=None, options=None):
        """
        run unit test ...
        TODO:
        """
        pass

    def package(self, config=None, options=None):
        """
        package code
        TODO:
        """
        pass

    def install(self, config=None, options=None):
        """
        install project as an artifact in local repository
        TODO:
        """
        pass

    def clean(self, config=None, options=None):
        """
        delete things in working folder
        """
        pass

    def deploy(self, config=None, options=None):
        """
        deploy packaged/built artifact to a remote node
        """
        pass

    def __getattribute__(self, name):
        attr = object.__getattribute__(self, name)
        stage = name
        if hasattr(attr, '__call__') and attr.__name__.upper() in BUILTIN_STAGES:
            def stage_interceptor(*args, **kwargs):
                #be careful
                kwargs["config"] = self.config

                def extension_executor(stg, extensions):
                    if stg in extensions:
                        for ext_name, ext in self.extensions[current_stage].iteritems():
                            logging.info('running %s' % ext_name)
                            print("=============================================")
                            print('%s: running %s' % (stg, ext_name))
                            print("=============================================")
                            try:
                                ext.do(*args, **kwargs)
                            except Exception as e:
                                self.failed_extensions[ext_name] = e
                                logging.error('Extension: %s failed in stage: %s' % (ext_name, stg))
                                logging.exception(e)

                current_stage = 'BEFORE_%s' % stage.upper()
                extension_executor(current_stage, self.extensions)

                print("=============================================")
                print("Stage: %s" % stage.upper())
                print("=============================================")
                result = attr(*args, **kwargs)

                current_stage = 'AFTER_%s' % stage.upper()
                extension_executor(current_stage, self.extensions)
                return result

            return stage_interceptor
        else:
            return attr


"""
Initiate Scripts
"""
# command option handling
parser = optparse.OptionParser()
parser.add_option("-c", "--command", dest="command",
                  help="build/install/deploy/package, required, command to be performed",
                  metavar="CMD")
(options, args) = parser.parse_args()

if not options.command:
    raise parser.error('Please specify a command')

build_mgr = GoBuildLifeCycleManager()


def help_cmd():
    print("Supported commands:")
    print(cmdRegister.keys())


cmdRegister = {
    "build": build_mgr.build,
    "install": build_mgr.install,
    "help": help_cmd
}

try:
    if options.command not in cmdRegister:
        help_cmd()
    else:
        cmd = cmdRegister[options.command]
        cmd(options=options)
        print("=============================================")
        if build_mgr.failed_extensions:
            print("Stage %s sucess with extension failures:" % options.command)
            while build_mgr.failed_extensions:
                name, e = build_mgr.failed_extensions.popitem()
                print(name)
        else:
            print('Stage Success: %s ' % options.command)
except Exception as e:
    logging.error('Stage failed: %s' % options.command)
    logging.exception(e)
